#!/bin/bash
docker exec django_sec-web-1 sh -c "./manage.py dumpdata --exclude auth.permission --exclude contenttypes > db.json"
docker cp django_sec-web-1:/usr/src/app/db.json .
