Django==4.2.6
psycopg2-binary==2.8.5
gunicorn==20.0.4
whitenoise==6.6.0