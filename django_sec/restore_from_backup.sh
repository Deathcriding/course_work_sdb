#!/bin/bash
docker cp ./db.json django_sec-web-1:/usr/src/app/
docker exec django_sec-web-1 sh -c "./manage.py loaddata ./db.json"

