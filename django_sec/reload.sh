#!/bin/bash
docker compose stop
docker rm django_sec-web-1
docker rmi django_sec-web
docker compose up -d
docker exec django_sec-web-1 python3.10 manage.py makemigrations
docker exec django_sec-web-1 python3.10 manage.py migrate
