from django.contrib import admin
from .models import departments, positions, persons, tasks, incidents

admin.site.register(departments)
admin.site.register(positions)
admin.site.register(persons)
admin.site.register(tasks)
admin.site.register(incidents)
