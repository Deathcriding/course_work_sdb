from .models import departments, positions, persons, tasks, incidents
from django.forms import ModelForm, TextInput, Textarea, NumberInput, Select
from django import forms

class departmnetsForm(ModelForm):
    class Meta:
        model = departments
        fields = ['departments_id','departments_description']
        widgets = {
            "departments_id": TextInput(attrs={
                'placeholder': 'Название отдела'}),
            "departments_description": Textarea(attrs={
                'placeholder': 'Описание отдела'})
        }


class positionsForm(ModelForm):
    class Meta:
        model = positions
        fields = ['positions_id', 'positions_description', 'positions_department', 'positions_acl']
        widgets = {
            "positions_id": TextInput(attrs={
                'placeholder': 'Название должности'}),
            "positions_description": Textarea(attrs={
                'placeholder': 'Описание должности'}),
            "positions_acl": NumberInput(attrs={
                'placeholder': 'Уровень доступа (число)'})
        }
class personsForm(ModelForm):
    class Meta:
        model = persons
        fields = ['username', 'persons_fio', 'persons_role', 'password']
        widgets = {
            "username": TextInput(attrs={
                'placeholder': 'Идентификатор'}),
            "person_fio": Textarea(attrs={
                'placeholder': 'ФИО'}),
            "password": TextInput(attrs={
                'type': 'password'})
        }

class tasksForm(ModelForm):
    class Meta:
        model = tasks
        fields = ['tasks_id', 'tasks_full_name', 'tasks_owner', 'tasks_description', 'tasks_check']
        widgets = {
            "tasks_id": TextInput(attrs={
                'placeholder': 'Короткое название'}),
            "tasks_full_name": TextInput(attrs={
                'placeholder': 'Полное название'}),
            "tasks_description": Textarea(attrs={
                'placeholder': 'Описание'}),
            "tasks_owner": Select(attrs={
                'id': 'my'}),
        }


class incidentsForm(ModelForm):
    class Meta:
        model = incidents
        fields = ['incidents_id', 'incidents_description', 'incidents_task', 'incidents_person']
        widgets = {
            "incidents_id": TextInput(attrs={
                'placeholder': 'Короткое название'}),
            "incidents_description": Textarea(attrs={
                'placeholder': 'Описание'})
        }

class LoginForm(forms.Form):
    username = forms.CharField(max_length=100)
    password = forms.CharField(max_length=100, widget=forms.PasswordInput())