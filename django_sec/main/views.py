from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, user_passes_test
from .forms import LoginForm
from django.http import HttpResponseForbidden
from django.shortcuts import render, redirect
from .models import departments, positions, persons, tasks, incidents
from .forms import departmnetsForm, positionsForm, personsForm, tasksForm, incidentsForm
from django.views.generic import DetailView, UpdateView, DeleteView


class DepartmentsDetail(DetailView):
    model = departments
    template_name = 'main/departments/departments_details.html'
    context_object_name = 'departments'
    def dispatch(self, request, *args, **kwargs):
        if request.user.username != "admin":
            return render(request, 'main/403.html')
        return super().dispatch(request, *args, **kwargs)




class DepartmentsUpdate(UpdateView):
    model = departments
    template_name = 'main/departments/departments_create.html'
    form_class = departmnetsForm
    def dispatch(self, request, *args, **kwargs):
        if request.user.username != "admin":
            return render(request, 'main/403.html')
        return super().dispatch(request, *args, **kwargs)

class DepartmentsDelete(DeleteView):
    model = departments
    template_name = 'main/departments/departments_delete.html'
    success_url = '/administrator/departments'
    def dispatch(self, request, *args, **kwargs):
        if request.user.username != "admin":
            return render(request, 'main/403.html')
        return super().dispatch(request, *args, **kwargs)

def index(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request,user)
                return redirect("/tasks")
            else:
                return redirect("/")
    else:
        if request.user.is_authenticated:
            logout(request)
        form = LoginForm()
        return render(request, 'main/index.html', {"form": form})

@login_required(login_url="/")
@user_passes_test(lambda u: u.username == 'admin')
def administrator (request):
    current_user = request.user
    return render(request, 'main/admin.html')


@login_required(login_url="/")
@user_passes_test(lambda u: u.username == 'admin')
def departments_return(request):
    departments_data = departments.objects.all()
    return render(request, 'main/departments/departments.html',{'departments_data': departments_data})

@login_required(login_url="/")
@user_passes_test(lambda u: u.username == 'admin')
def departments_create(request):
    error = ''
    if request.method == 'POST':
        form = departmnetsForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('departments')
        else:
            error = 'The form is invalid'
    form = departmnetsForm()
    data = {
        'form': form,
        'error': error
    }
    return render(request, 'main/departments/departments_create.html', data)



class PositionsDetail(DetailView):
    model = positions
    template_name = 'main/positions/positions_details.html'
    context_object_name = 'positions'
    def dispatch(self, request, *args, **kwargs):
        if request.user.username != "admin":
            return render(request, 'main/403.html')
        return super().dispatch(request, *args, **kwargs)


class PositionsUpdate(UpdateView):
    model = positions
    template_name = 'main/positions/positions_create.html'
    form_class = positionsForm
    def dispatch(self, request, *args, **kwargs):
        if request.user.username != "admin":
            return render(request, 'main/403.html')
        return super().dispatch(request, *args, **kwargs)

class PositionsDelete(DeleteView):
    model = positions
    template_name = 'main/positions/positions_delete.html'
    success_url = '/administrator/positions'
    def dispatch(self, request, *args, **kwargs):
        if request.user.username != "admin":
            return render(request, 'main/403.html')
        return super().dispatch(request, *args, **kwargs)

@login_required(login_url="/")
@user_passes_test(lambda u: u.username == 'admin')
def positions_return(request):
    positions_data = positions.objects.all()
    return render(request, 'main/positions/positions.html',{'positions_data': positions_data})

@login_required(login_url="/")
@user_passes_test(lambda u: u.username == 'admin')
def positions_create(request):
    error = ''
    if request.method == 'POST':
        form = positionsForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('positions')
        else:
            error = 'The form is invalid'
    form = positionsForm()
    data = {
        'form': form,
        'error': error
    }
    return render(request, 'main/positions/positions_create.html', data)



class PersonsDetail(DetailView):
    model = persons
    template_name = 'main/persons/persons_details.html'
    context_object_name = 'persons'
    def dispatch(self, request, *args, **kwargs):
        if request.user.username != "admin":
            return render(request, 'main/403.html')
        return super().dispatch(request, *args, **kwargs)


class PersonsUpdate(UpdateView):
    model = persons
    template_name = 'main/persons/persons_create.html'
    form_class = personsForm
    def dispatch(self, request, *args, **kwargs):
        if request.user.username != "admin":
            return render(request, 'main/403.html')
        return super().dispatch(request, *args, **kwargs)

class PersonsDelete(DeleteView):
    model = persons
    template_name = 'main/persons/persons_delete.html'
    success_url = '/administrator/persons'
    def dispatch(self, request, *args, **kwargs):
        if request.user.username != "admin":
            return render(request, 'main/403.html')
        return super().dispatch(request, *args, **kwargs)
@login_required(login_url="/")
@user_passes_test(lambda u: u.username == 'admin')
def persons_return(request):
    persons_data = persons.objects.all()
    return render(request, 'main/persons/persons.html',{'persons_data': persons_data})

@login_required(login_url="/")
@user_passes_test(lambda u: u.username == 'admin')
def persons_create(request):
    error = ''
    if request.method == 'POST':
        form = personsForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('persons')
        else:
            error = 'The form is invalid'
    form = personsForm()
    data = {
        'form': form,
        'error': error
    }
    return render(request, 'main/persons/persons_create.html', data)


class TasksDetail(DetailView):
    model = tasks
    template_name = 'main/tasks/tasks_details.html'
    context_object_name = 'tasks'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['current_user'] = self.request.user
        context['acl'] = self.request.user.persons_role.positions_acl
        return context


class TasksUpdate(UpdateView, DetailView):
    model = tasks
    template_name = 'main/tasks/tasks_edit.html'
    form_class = tasksForm
    context_object_name = 'tasks'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['current_user'] = self.request.user
        return context

class TasksDelete(DeleteView):
    model = tasks
    template_name = 'main/tasks/tasks_delete.html'
    success_url = '/tasks'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['current_user'] = self.request.user
        context['acl'] = self.request.user.persons_role.positions_acl
        return context
@login_required(login_url="/")
def tasks_return(request):
    tasks_data = tasks.objects.all()
    current_user = request.user
    if str(request.user) != 'admin':
        acl = request.user.persons_role.positions_acl
    else:
        acl = 10000000000000000000
    return render(request, 'main/tasks/tasks.html',{'tasks_data': tasks_data, 'current_user': current_user, "acl": acl})

@login_required(login_url="/")
def tasks_create(request):
    error = ''
    current_user = request.user
    acl = request.user.persons_role.positions_acl
    if request.method == 'POST':
        form = tasksForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('tasks')
        else:
            error = 'The form is invalid'
    form = tasksForm(initial={'tasks_owner': request.user})
    data = {
        'form': form,
        'error': error,
        'current_user': current_user,
        'acl': acl,
    }
    return render(request, 'main/tasks/tasks_create.html', data)


class IncidentsDetail(DetailView):
    model = incidents
    template_name = 'main/incidents/incidents_details.html'
    context_object_name = 'incidents'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['current_user'] = self.request.user
        return context


class IncidentsUpdate(UpdateView):
    model = incidents
    template_name = 'main/incidents/incidents_create.html'
    form_class = incidentsForm
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['current_user'] = self.request.user
        return context

class IncidentsDelete(DeleteView):
    model = incidents
    template_name = 'main/incidents/incidents_delete.html'
    success_url = '/incidents'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['current_user'] = self.request.user
        return context
@login_required(login_url="/")
def incidents_return(request):
    incidents_data = incidents.objects.all()
    current_user = request.user
    return render(request, 'main/incidents/incidents.html',{'incidents_data': incidents_data, 'current_user': current_user})

@login_required(login_url="/")
def incidents_create(request):
    error = ''
    current_user = request.user
    if request.method == 'POST':
        form = incidentsForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('incidents')
        else:
            error = 'The form is invalid'
    form = incidentsForm()
    data = {
        'form': form,
        'error': error,
        'current_user': current_user
    }
    return render(request, 'main/incidents/incidents_create.html', data)

