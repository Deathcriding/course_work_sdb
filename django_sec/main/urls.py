from django.urls import path
from . import views
from .views import *
urlpatterns = [
    path('', views.index, name='home'),
    path('administrator', views.administrator, name='administrator'),
    path('administrator/departments', views.departments_return, name='departments'),
    path('administrator/create_departments', views.departments_create, name='create_departments'),
    path('administrator/departments/<str:pk>', login_required(views.DepartmentsDetail.as_view(template_name="main/departments/departments_details.html")), name='department_detail'),
    path('administrator/departments/<str:pk>/update', login_required(views.DepartmentsUpdate.as_view(template_name="main/departments/departments_create.html")), name='department_update'),
    path('administrator/departments/<str:pk>/delete', login_required(views.DepartmentsDelete.as_view(template_name="main/departments/departments_delete.html")), name='department_delete'),
    path('administrator/positions', views.positions_return, name='positions'),
    path('administrator/create_positions', views.positions_create, name='create_positions'),
    path('administrator/positions/<str:pk>', login_required(views.PositionsDetail.as_view(template_name="main/positions/positions_details.html")), name='position_detail'),
    path('administrator/positions/<str:pk>/update', login_required(views.PositionsUpdate.as_view(template_name="main/positions/positions_create.html")), name='position_update'),
    path('administrator/positions/<str:pk>/delete', login_required(views.PositionsDelete.as_view(template_name="main/positions/positions_delete.html")), name='position_delete'),
    path('administrator/persons', views.persons_return, name='persons'),
    path('administrator/create_persons', views.persons_create, name='create_persons'),
    path('administrator/persons/<str:pk>', login_required(views.PersonsDetail.as_view(template_name="main/persons/persons_details.html")), name='person_detail'),
    path('administrator/persons/<str:pk>/update',login_required(views.PersonsUpdate.as_view(template_name="main/persons/persons_create.html")),name='person_update'),
    path('administrator/persons/<str:pk>/delete', login_required(views.PersonsDelete.as_view(template_name="main/persons/persons_delete.html")), name='person_delete'),
    path('tasks', views.tasks_return, name='tasks'),
    path('create_tasks', views.tasks_create, name='create_tasks'),
    path('tasks/<str:pk>', login_required(views.TasksDetail.as_view(template_name="main/tasks/tasks_details.html")), name='task_detail'),
    path('tasks/<str:pk>/update',login_required(views.TasksUpdate.as_view(template_name="main/tasks/tasks_edit.html")),name='task_update'),
    path('tasks/<str:pk>/delete', login_required(views.TasksDelete.as_view(template_name="main/tasks/tasks_delete.html")), name='task_delete'),
    path('incidents', views.incidents_return, name='incidents'),
    path('create_incidents', views.incidents_create, name='create_incidents'),
    path('incidents/<str:pk>', login_required(views.IncidentsDetail.as_view(template_name="main/incidents/incidents_details.html")), name='incident_detail'),
    path('incidents/<str:pk>/update', login_required(views.IncidentsUpdate.as_view(template_name="main/incidents/incidents_create.html")), name='incident_update'),
    path('incidents/<str:pk>/delete', login_required(views.IncidentsDelete.as_view(template_name="main/incidents/incidents_delete.html")), name='incident_delete'),

]
