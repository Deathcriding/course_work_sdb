from django.contrib.auth.models import AbstractUser, PermissionsMixin
from django.db import models

class departments(models.Model):
    departments_id = models.CharField('Наазвание', max_length=100, primary_key=True)
    departments_description = models.TextField('Описание')

    def __str__(self):
        return self.departments_id

    def get_absolute_url(self):
        return f'/administrator/departments/{self.departments_id}'
    class Meta:
        verbose_name = 'Department'
        verbose_name_plural = "Departments"

class positions(models.Model):
    positions_id = models.CharField('Наазвание', max_length=100, primary_key=True)
    positions_description = models.TextField('Описание')
    positions_department = models.ForeignKey(departments, on_delete=models.CASCADE)
    positions_acl = models.IntegerField('Уровень доступа', default=0)

    def __str__(self):
        return self.positions_id

    def get_absolute_url(self):
       return f'/administrator/positions/{self.positions_id}'

    class Meta:
        verbose_name = 'Должность'
        verbose_name_plural = "Должности"

class persons(AbstractUser, PermissionsMixin):
    username = models.CharField('Имя пользователя', max_length=100, primary_key=True)
    persons_fio = models.CharField('ФИО', max_length=150, null=True)
    persons_role = models.ForeignKey(positions, on_delete=models.CASCADE, null=True)
    password = models.CharField(max_length=100)
    def save(self, *args, **kwargs):
        self.set_password(self.password)
        super().save(*args, **kwargs)
        if self._password is not None:
            self._password = None
   # def save(self,*args,**kwargs):
   #     if self.password:
   #         self.set_password(self.password)
   #     super().save(*args,**kwargs)
    def __str__(self):
        return self.username

    def get_absolute_url(self):
       return f'/administrator/persons/{self.username}'
    class Meta:
        verbose_name = 'Сотрудник'
        verbose_name_plural = "Сотрудники"

@property
def is_authenticated(self):
    return True

class tasks(models.Model):
    tasks_id = models.CharField('Короткое название', max_length=100, primary_key=True)
    tasks_full_name = models.CharField('Полное название', max_length=255)
    tasks_owner = models.ForeignKey(persons, on_delete=models.CASCADE)
    tasks_description = models.TextField('Описание задачи')
    tasks_check = models.BooleanField('Статус о выполнении')
    def __str__(self):
        return self.tasks_id

    def get_absolute_url(self):
       return f'/tasks/{self.tasks_id}'
    class Meta:
        verbose_name = 'Задача'
        verbose_name_plural = "Задачи"

class incidents(models.Model):
    incidents_id = models.CharField('Название', max_length=100, primary_key=True)
    incidents_description = models.TextField('Описание инцидента')
    incidents_task = models.ForeignKey(tasks, on_delete=models.CASCADE)
    incidents_person = models.ForeignKey(persons, on_delete=models.CASCADE)
    def __str__(self):
        return self.incidents_id

    def get_absolute_url(self):
       return f'/incidents/{self.incidents_id}'
    class Meta:
        verbose_name = 'Инцидент'
        verbose_name_plural = "Инциденты"